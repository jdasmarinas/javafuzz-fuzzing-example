# Fuzzing for Java Example

## test

This is an example of how to integrate your [javafuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/javafuzz/-/tree/master) targets into GitLab CI/CD

This example will show the following steps:
* [Running the javafuzz target via GitLab CI/CD](#running-javafuzz-fuzz-from-ci)
* [Building and running a simple javafuzz target locally](#building-and-running-a-javafuzz-target)

Result:
* javafuzz targets will run a test on the master branch on every commit.
* javafuzz targets will run regression tests on every merge request (and every other branch) with the generated corpus and crashes to catch bugs early on.

Fuzzing for Java can help find both complex bugs and correctness bugs. Java is a safe language so memory corruption bugs
are unlikely to happen, but bugs can still have security [implications](https://blog.cloudflare.com/dns-parser-meet-go-fuzzer/) and
cause the app to fail in production.

This tutorial focuses less on how to build javafuzz targets and more on how to integrate the targets with GitLab. A lot of 
great information is available at the [javafuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/javafuzz) repository.

## Running javafuzz from CI

The best way to integrate javafuzz fuzzing with Gitlab CI/CD is by adding additional stage & step to your `.gitlab-ci.yml`.

```yaml

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml

my_fuzz_target:
  extends: .fuzz_base
  script:
    - mvn install
    - ./gitlab-cov-fuzz run --regression=$REGRESSION --engine javafuzz -- com.gitlab.javafuzz.examples.FuzzParseComplex
```

For each fuzz target you will will have to create a step which extends `.fuzz_base` that runs the following:
* Builds the fuzz target
* Runs the fuzz target via `gitlav-cov-fuzz` CLI.
* Choose the `javafuzz` fuzz engine specifically (instead of another, like [JQF](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing/java-fuzzing-example)), with `--engine javafuzz`
* For `$CI_DEFAULT_BRANCH` (can be override by `$COV_FUZZING_BRANCH`) will run fully fledged fuzzing sessions.
  For everything else including MRs will run fuzzing regression with the accumlated corpus and fixed crashes.  

## Building and running a javafuzz target

The example is located in the [javafuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/javafuzz) repository.
